#' view_rename 
#'
#' @description A fct function
#'
#' @return The return value, if any, from executing the function.
#'
#' @noRd

view_rename <- function(vals, input, output, session) {
  # print(vals$curr.fields$Field1)
  
  output$rename_verify_label <- renderUI({
    h3(HTML('<b>1. Verify Project</b>'))
  })
  
  ## Fill project options
  observe(
    updateSelectInput(session = session, inputId = 'project_rename',
                      choices = c(' ' = NA, setNames(vals$fieldstbl$Project, vals$fieldstbl$ProjectName)))
  )
  
  #### ****UNCOMMENT THESE IF YOU WANT USERS TO SELECT PHOTOS TO RENAME AT A LEVEL BELOW PROJECT_COPY ****####
  ## Render
  # observeEvent(input$project_rename, {
  #     # req(valid(input$project_rename))
  #     # req(valid(vals$field1label_rename))
  #   
  #     output$field1_rename <- renderUI(
  #       if (valid(vals$field1label_rename)) {  
  #         selectizeInput(inputId = "field1_rename",
  #         label = HTML(paste0('<font size=4>', vals$field1label_rename, '</font>')),
  #         choices = c(' ' = NA, setNames(vals$rename.options$Var1, vals$rename.options$Name1)),
  #         options = list(create = TRUE))      
  #       } else {
  #         h6('')
  #       }
  #     )
  # })
  
  ##' I'm going to leave te ifelse statements with field1_rename in here, because 
  ##' as it is they'll just be skipped, but if you want to add selection back in
  ##' below the project level for selecting which ones to rename, then they'll just be here
  output$batch_hint <- shiny::renderUI({
    # req(valid(input$field1_rename) | (!valid(vals$field1label_rename) & valid(input$project_rename)))
    req(valid(vals$prename))
    h5(HTML(paste0('<ul style = "list-style-type:circle;">
                    <li>Select the folder <b>',
                   input$project_rename, '\\<wbr>', input$project_rename, '_copy', ifelse(valid(vals$field1_rename), paste0('\\<wbr>', vals$field1_rename), ''),
                   '</b> created by the app on your external hard drive</li></ul>')))
  })
  
  output$rename_hint <- shiny::renderUI({
    ## UNCOMMENT BELOW LINE TO SWITCH BACK TO 'OLD VERSION'
    # req(valid(input$field1_rename) | (!valid(vals$field1label_rename) & valid(input$project_rename)))
    req(valid(input$project_rename))
    h5(HTML(paste0("<ul style=\"list-style-type:circle;\">
                    <li>Select the <b>same hard drive (e.g., E:\\ or F:\\)</b> you chose for copying photos (app will create folder structure <b>",
                    input$project_rename,"\\<wbr>", vals$prename,
                    ifelse(valid(vals$field1label_rename),
                           paste0('\\<wbr>[', vals$field1label_rename, ']'),
                           ''),
                    ifelse(valid(vals$field2label_rename),
                           paste0('\\<wbr>[', vals$field2label_rename, ']'),
                           ''),
                    ifelse(valid(vals$field3label_rename),
                           paste0('\\<wbr>[', vals$field3label_rename, ']'),
                           ''),
                    ifelse(valid(vals$field4label_rename),
                           paste0('\\<wbr>[', vals$field4label_rename, ']'),
                           ''),
                    "\\[CellID]</b> on hard drive)</li></ul>")))
  })
}