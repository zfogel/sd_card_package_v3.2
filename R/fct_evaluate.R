#' evaluate 
#'
#' @description A fct function for evaluating the camera 
#' Page for user to look through photos and assess the camera's performance, then
#' saves that evaluation to the metadata file
#'
#' @return The return value, if any, from executing the function.
#'
#' @noRd

evaluateR <- function(vals, input, output, session) {
  
  output$submission <- shiny::renderUI({
    ## check to make sure that user has selected something
    if (input$submitData == 0 | is.null(input$badreason) | length(input$badreason) == 0) {
      return()
    } else {
            shiny::isolate({
              shiny::withProgress(
                                  value = 0,
                                  {
                                   incProgress(6/10, message = 'Updating metadata...')
                                    ## this is the same as in the copy tab
                                    if (stringr::str_sub(input$selectedBackup, start=-1) == '\\'){
                                      backup <- gsub('\\\\', '', input$selectedBackup) }
                                    else backup <- gsub('\\\\', '/', input$selectedBackup)
                                    
                                    ## store project folder path as variable
                                    pfolder <- file.path(backup, vals$pcode) # eg F:/SWWLF
                                    
                                    ## creates filepath for metadata file
                                    ## eg F:/SWWLF/SWWLF2021_R1_MasterMetadata.csv
                                    csvfile <- file.path(pfolder, paste0(vals$file_prefix, '_MasterMetadata.csv'))
                                    print(csvfile)
                                    ##' selects just a few fields to display
                                    ##' (vals$metadf was created in copy tab; it's
                                    ##' one of the fiew vals$objects that's assigned
                                    ##' outside of assign.reactives)
                                    metadf <- vals$metadf %>%
                                      dplyr::select(Project, Field1, CellID, CamID, 
                                                    StartDate, EndDate, FixID, 
                                                    BadReason) %>%
                                      dplyr::distinct()
                                    
                                    ##' depending on whether Field1 is defined for
                                    ##' that project, either change the name to 
                                    ##' the actual label or remove that field
                                    if (valid(vals$curr.fields$Field1)) {
                                      # change 'Field1' to actual name
                                      names(metadf)[2] <- vals$curr.fields$Field1 
                                    } else {
                                      # remove Field1
                                      metadf <- metadf %>%
                                        dplyr::select(-Field1)
                                    }
                                    
                                    ## assign BadReason field
                                    metadf$BadReason <- vals$badreason
                                    
                                    ## if metadata file exists, write it
                                    if (!file.exists(csvfile)) {
                                      ##' create a file with the name/location
                                      ##' specified by csvfile, and the file itself
                                      ##' is csvhead
                                      write.csv(metadf, csvfile, row.names = F)
                                    } else {
                                      ##' if that csv file already exists, then 
                                      ##' append data for current cam on the end
                                      ##' of it
                                      write.table(metadf, csvfile, append = T, 
                                                  row.names = F, col.names = F, sep = ',')
                                    }
                                    
                                    ##' success message
                                    list(h1(HTML("<center><font color=\"#3c8dbc\" style = \"text-shadow: 1.5px 1.5px #4d3a7d\"><b>THANKS FOR EVALUATING THIS CAMERA</b></font></center>")),
                                                 h4(HTML("If it's time to move on to the next camera, click the <b>SELECT New Camera</b> button below")),
                                                 actionButton(inputId = "evaluate_restart",label = HTML("<font size = 4><b>SELECT New Camera</b></font>"), 
                                                              style="color: #fff; background-color: #6faade; border-color: #5491c7; width: 100%"),
                                                 br(),br(),
                                                 h4(HTML("If it's time to rename a batch of photos, click the <b>RENAME All Photos</b> button below")),
                                                 h5(HTML("To save yourself some time, save this step until you have multiple cameras copied and ready for renaming,
                                                          and run at the end of the day")),
                                                 actionButton(inputId = "evaluate_rename",label = HTML("<font size = 4><b>RENAME All Photos</b></font>"), 
                                                              style="color: #fff; background-color: #6faade; border-color: #5491c7; width: 100%")
                                            )
                                  }) # withProgress
                          }) # isolate
           } # else (input$submitData != 0)
  }) # renderUI (output$submission)
  
  ## back to select tab if user wants to copy more
  shiny::observeEvent(input$evaluate_restart, {
    reset(input, output, session)
    shiny::updateTabsetPanel(session, 'tabs', selected = 'select')
  })
  
  ## on to rename tab if user wants to rename photos
  shiny::observeEvent(input$evaluate_rename, {
    reset(input, output, session)
    shiny::updateTabsetPanel(session, 'tabs', selected = 'rename')
  })
}










