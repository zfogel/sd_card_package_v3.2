#' The application server-side
#' 
#' @param input,output,session Internal parameters for {shiny}. 
#'     DO NOT REMOVE.
#'     
#' I'm putting this here because I'm not sure where else to put it; but this is 
#' my stackoverflow account; I've posted a lot of questions about shiny stuff 
#' so it may be helpful. You can't make fun of my username though, I made it in 
#' college because I decided it was my trail/cowboy name
#'  https://stackoverflow.com/users/11186959/slim
#'  
#' @import shiny
#' @noRd
app_server <- function( input, output, session ) {
  
  ## Adding in a delay-- this will hopefully solve the lag in the select tab where
  ## some of the dropdown menus don't appear
  shinybusy::show_modal_spinner()
  Sys.sleep(10)
  # Your application server logic 
  shinybusy::remove_modal_spinner()
  
  vals <- assign.reactives(input, output, session)
  
  view_start(input, output, session)
  view_select(vals, input, output, session)
  view_copy(vals, input, output, session)
  view_evaluate(vals, input, output, session)
  view_rename(vals, input, output, session)
  
  copySD(vals, input, output, session)
  evaluateR(vals, input, output, session)
  renameR(vals, input, output, session)
  fixID(vals, input, output, session)
  
}
