#' rename 
#'
#' @description A fct function
#'
#' @return The return value, if any, from executing the function.
#' Renames photos after they've been copied. It does so by calling an exiftool 
#' command via system2()/command line. Generally this is done as a batch. 
#' Helper functions in fct_rename_helpers.R
#' 
#' The following pages were useful for learning how exiftool/command line work
#' https://exiftool.org/exiftool_pod.html
#' https://ninedegreesbelow.com/photography/exiftool-commands.html
#' https://www.digitalcitizen.life/command-prompt-how-use-basic-commands/
#'
#'
#' @noRd 

## brings in vals from assign.reactives()
renameR <- function(vals, input, output, session) {
  # choose folder with original photos
  #' input$chooseBatch is folder selection popup
  shiny::observeEvent(input$chooseBatch, {
    #' choose.dir() initializes to no folder
    orig_folder <- choose.dir('')
    if (!is.na(orig_folder)) {
      output$selectedBatch <- shiny::renderUI({
        #' checkbox to show user which folder they selected
        checkboxGroupInput(inputId = 'selectedBatch', label = '',
                           choices = orig_folder, selected = orig_folder)
      })
    } else ('Folder selection cancelled.')
  }) # observeEvent
  
  # choose folder for renamed photos 
  # same as chooseBatch above
  shiny::observeEvent(input$chooseRename, {
    rename_folder <- choose.dir('')
    if (!is.na(rename_folder)) {
      output$selectedRename <- shiny::renderUI({
        checkboxGroupInput(inputId = 'selectedRename', label='',
                           choices = rename_folder, selected = rename_folder)
      })
    } else ('Folder selection cancelled.')
  })
  
  ## "Rename photos" button clicked
  observeEvent(input$rename, {
    output$rename_message <- shiny::renderUI({
      #' this is to make sure they actually clicked it
      if (input$rename == 0) {
        return()
      } else {
        # shiny::isolate({
        shiny::withProgress(
          value = 0, {
            
            incProgress(2/10, message = 'Finding photos to rename...')
            
            #' generate labels
            #' create error file/parent folder as necessary
            #' remove blackslashes from name, not totally sure why this is necessary
            if (stringr::str_sub(input$selectedRename, start=-1)=='\\') {
              backup <- gsub('\\\\', '', input$selectedRename)
            } else backup <- gsub('\\\\', '/', input$selectedRename)
            
            if (stringr::str_sub(input$selectedBatch, start=-1)=='\\') {
              source <- gsub('\\\\', '', input$selectedBatch)
            } else source <- gsub('\\\\', '/', input$selectedBatch)
            
            #' make variable with name of backup drive and project, eg F:/SWWLF2021
            pfolder <- file.path(backup, input$project_rename)
            #' create the folder in the backup drive if it doesn't exist
            if(!dir.exists(pfolder)) {
              dir.create(pfolder)
            }
            # print('pfolder created')
            #' make txt file for tracking renamed files, eg F:/SWWLF2021/SWWLF2021_RenameLog.txt
            rename_log <- file.path(pfolder, paste0(input$project_rename, '_RenameLog.txt'))
            #' create rename log file if it doesn't exist
            if (!file.exists(rename_log)) {
              file.create(rename_log)
            }
            
            #' goes into _copy folder, takes the existing folder structure, and 
            #' then moves that into the _rename folder
            copydirs <- list.dirs(path = source)
            #' make a second copy for down below
            copydirs2 <- copydirs
            #' replace backslashes with forward slashes
            copydirs <- gsub('\\\\', '/', copydirs)
            #' remove 100RECNX folders from list 
            #' The reason we duplicated copydirs is because we need to have a 
            #' version with 100RECNX etc retained so the app knows where to look
            #' in the project_copy folder, but we need to take out 100RECNX for 
            #' the project_rename ones
            copydirs <- copydirs[!grepl('\\d+RECNX', copydirs)] 
            #' make identical/corresponding set of files, but with rename instead 
            #' of copy eg F:/SWWLF2021/SWWLF2021_rename/CellID/CamID
            renamedirs <- gsub('copy', 'rename', copydirs)
            
            #' need to have all folders (ie SWWLF_rename, R1, A, etc) here 
            #' because it can't create a subfolder if the larger one doesn't exist
            lapply(file.path(backup, input$project_rename, vals$prename),
                   # makes rename folder
                   function(x) if(!dir.exists(x)) dir.create(file.path(x))) 
            lapply(renamedirs, function(x) if(!dir.exists(x)) dir.create(file.path(x)))
            
            #' now remove all directories except for the lowest subfolder (the 
            #' ones immediately containing pictures; ie 100RECNX)
            #' need to go back to these to make sure that the copydirs2 is actually 
            #' needed
            copydirs <- copydirs2[grepl('\\d+RECNX', copydirs2)]
            copydirs <- gsub('/\\d+RECNX', '', copydirs)
            renamedirs <- gsub('copy', 'rename', copydirs)
            
            # list all photo files to be renamed
            files <- list.files(input$selectedBatch, pattern = '*.JPG', 
                                full.names = T, recursive = T, include.dirs = F) 
            nf <- length(files)
            # print(paste('nf', nf))
            #' in the event that there are no photos in the copy folder to be renamed
            if (nf == 0) {
              list(h1(HTML('<center><font color="#3c8dbc" style="text-shadow: 1.5px 1.5px #4d3a7d"><b>OOPS!</b></font></center>')),
                   br(),
                   h4(HTML('There are no photos to rename. Please check the folder path for your photos.')))
            } else {
              incProgress(3/10, message = 'Renaming photos...')
              
              #' helper function to actually rename photos (function defined in 
              #' fct_rename_helpers.R)
              #' 'One' means that it's round 1
              rename_exif(renamedirs, copydirs, rename_log, 'One', input, vals)
              
              incProgress(4/10, message = 'Checking for errors...')
              #' check to see if any photos weren't renamed and are left behind
              #' in project_copy folder (the way photos are renamed in the 
              #' rename_exif function means that the original version is deleted)
              left.files <- list.files(input$selectedBatch, pattern = '*JPG$', 
                                       recursive = T, full.names = T) 
              lf <- length(left.files)
              
              #' list all the directories in the copy folder
              left.dirs <- list.dirs(input$selectedBatch)
              
              # print(lf)
              
              #' delete empty folders in copy, display table of renamed info, 
              #' print 'success' output to app UI
              if (lf == 0) {
                
                unlink(left.dirs, recursive = T) # unlink() deletes folders
                #' renamesuccess() and renameoutputsuccess are defined in rename_helpers
                renamesuccess(input, output, session, vals, files, backup)
                renameoutputsuccess(input, output, session)
              }
              else { # lf > 0
                #### SECONDARY RENAME ----
                
                #' run rename_exif again with round 2
                rename_exif(renamedirs, copydirs, rename_log, 'Two', input, vals)
                
                #' list files again
                left.files_2 <- list.files(input$selectedBatch, pattern = '*.JPG$', 
                                           full.names = T, recursive = T) ## look for any remaining photos
                lf_2 <- length(left.files_2)
                left.dirs_2 <- list.dirs(input$selectedBatch)
                
                if (lf_2 == 0) {
                  unlink(left.dirs_2, recursive = T) # unlink() deletes folders
                  renamesuccess(input, output, session, vals, files, backup)
                  renameoutputsuccess(input, output, session)
                } # if (lf_2 == 0)
                else { # lf_2 > 0
                  invisible(lapply(left.dirs_2, function(x) {
                    #' get file info for all remaining directories
                    #' returns dataframe
                    #' x is left.dirs_2
                    fi <- file.info(x) 
                    if(is.na(fi$isdir) | fi$isdir == T) {
                      f <- list.files(x, all.files = T, recursive = T, full.names = T)
                      #' if files exist but don't contain anything then they
                      #' can be deleted
                      sz <- sum(file.info(f)$size)
                      #' delete files if they're all empty
                      if (sz==0L) unlink(x, recursive = T)
                    } # if
                  } # function(x)
                  ) # lapply
                  ) # invisible
                  renamedonemessage()
                  renamesuccess(input, output, session, vals, files, backup)
                  #' might want to change this so if those photo files were all
                  #' empty and thus deleted, this displays normal success output,
                  #' but if they weren't empty then displays the error output
                  list(h1(HTML('<center><font color="#3c8dbc" style = "text-shadow: 1.5px 1.5px #4d3a7d"><b>HMM...</b></font></center>')),
                       br(),
                       h4(HTML(paste0('You finished renaming and moving photos from <font face="Courier New>"',
                                      source, '</font> to <font face="Courier New">', paste(vals$folderpath_rename, collapse = '/<wbr>'),
                                      '</font>, but there are still <b>', lf_2,
                                      ' photos left</b> in the original folder.'))),
                       br(),
                       h4(HTML(paste0('</b>Please check <font face="Courier New">', rename_log,
                                      '</font> for information regarding this error, and/or ',
                                      'check <b>', source, '</b> to see which photos didn\'t copy.',
                                      'You can also <a href="mailto:sarah.thompson@idfg.idaho.gov?subject=SD Card App error: some photos not being renamed">',
                                      'send us the file</a> for troubleshooting.'))),
                       h4(HTML('When you\'re ready to move on, hit the <b>SELECT New Camera</b> button below')),
                       br(),
                       #' button to go back to select new camera
                       actionButton(inputId = 'rename_restart',
                                    label = HTML('<font size = 4><b>SELECT New Camera</b></font>'),
                                    class = 'btn-dark')
                  ) # list
                } # else (lf_2 > 0)
              } # else (lf > 0)
            } # else (nf > 0)
          }) # withProgress
        # }) # isolate
      } # input$rename clicked
    }) # renderUI
  }) # observeEvent(input$rename)
  
  shiny::observeEvent(input$rename_restart, {
    shiny::updateTabsetPanel(session=session, "tabs",
                             selected = 'select')
    reset(input, output, session)
    # rename_reset(input, output, session)
    # shiny::updateSelectInput(session, 'project', selected = character(0))
    # shiny::updateTextInput(session, "cell", value = "")
    # shiny::updateTextInput(session, "camid", value = "")
    # shiny::updateTextInput(session, "ID", value = "")
    # shiny::updateSelectInput(session, "field1", selected=character(0))
    # shiny::updateSelectInput(session, "field2", selected=character(0))
    # shiny::updateSelectInput(session, "field3", selected=character(0))
    # shiny::updateSelectInput(session, "field4", selected=character(0))
  })
}